package com.rubenescudero.unidad2;

import java.sql.*;

/**
 * Clase modelo donde ejecutamos las consultas de la base de datos
 */
public class Modelo {
    private Connection connection;

    /**
     * Metodo para conectar con la base de datos
     * @throws SQLException
     */
    public void conectar() throws SQLException {
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/lista_videojuegos", "root", "");
    }

    /**
     * Metodo para desconectarnos de la base de datos
     * @throws SQLException
     */
    public void desconectar() throws SQLException {
        connection.close();
        connection = null;
    }

    /**
     * Metodo para obtener los datos de la base de datos
     * @return devuelve un ResultSet con los datos de la base de datos
     * @throws SQLException
     */
    public ResultSet obtenerDatos() throws SQLException {
        if(connection == null) {
            return null;
        }
        if(connection.isClosed()) {
            return null;
        }
        String consulta = "SELECT * FROM videojuegos";
        PreparedStatement sentencia = connection.prepareStatement(consulta);
        ResultSet resultSet = sentencia.executeQuery();
        return resultSet;
    }

    /**
     * Metodo para buscar en la base de datos por un nombre
     * @param nombre indica el nombre que se va a buscar en la base de datos
     * @return devuelve la fila que contenga el nombre buscado en la base de datos
     * @throws SQLException
     */
    public ResultSet buscarPorNombre(String nombre) throws SQLException {
        if(connection == null) {
            return null;
        }
        if(connection.isClosed()) {
            return null;
        }
        String consulta = "SELECT * FROM videojuegos WHERE nombre like " + "\'" + nombre + "\'";
        PreparedStatement sentencia = connection.prepareStatement(consulta);
        ResultSet resultSet = sentencia.executeQuery();
        return resultSet;
    }

    /**
     * Metodo para buscar en la base de datos por un id
     * @param id indica el id que se va a buscar en la base de datos
     * @return devuelve la fila que contenga el id buscado en la base de datos
     * @throws SQLException
     */
    public ResultSet buscarPorId(String id) throws SQLException {
        if(connection == null) {
            return null;
        }
        if(connection.isClosed()) {
            return null;
        }
        String consulta = "SELECT * FROM videojuegos WHERE id = " + id;
        PreparedStatement sentencia = connection.prepareStatement(consulta);
        ResultSet resultSet = sentencia.executeQuery();
        return resultSet;
    }

    /**
     * Metodo para hace una busqueda por orden alfabetico de los nombres de los videojuegos
     * @return devuelve los datos de la base de datos ordenados alfabeticamente
     * @throws SQLException
     */
    public ResultSet ordernarAlfabeticamente() throws SQLException {
        if(connection == null) {
            return null;
        }
        if(connection.isClosed()) {
            return null;
        }
        String consulta = "SELECT * FROM videojuegos ORDER BY nombre ASC";
        PreparedStatement sentencia = connection.prepareStatement(consulta);
        ResultSet resultSet = sentencia.executeQuery();
        return resultSet;
    }

    /**
     * Metodo para hacer una consulta de los videojuegos por su desarrolladora
     * @return devuelve la consulta para ver los videojuegos de la base de datos ordenados por desarrolladora
     * @throws SQLException
     */
    public ResultSet ordernarDesarrolladora() throws SQLException {
        if(connection == null) {
            return null;
        }
        if(connection.isClosed()) {
            return null;
        }
        String consulta = "SELECT * FROM videojuegos ORDER BY desarrolladora";
        PreparedStatement sentencia = connection.prepareStatement(consulta);
        ResultSet resultSet = sentencia.executeQuery();
        return resultSet;
    }

    /**
     * Metodo para hacer una consulta para ordenar los videojuegos por precio
     * @return devuelve los videojuegos ordenados por precio
     * @throws SQLException
     */
    public ResultSet ordernarPrecio() throws SQLException {
        if(connection == null) {
            return null;
        }
        if(connection.isClosed()) {
            return null;
        }
        String consulta = "SELECT * FROM videojuegos ORDER BY nombre ASC";
        PreparedStatement sentencia = connection.prepareStatement(consulta);
        ResultSet resultSet = sentencia.executeQuery();
        return resultSet;
    }

    /**
     * Metodo para insertar nuevos videojuegos en la base de datos
     * @param nombre indica el nombre del videojuego
     * @param desarrolladora indica el nombre de la desarrolladora
     * @param genero indica el genero al que pertenece el videojuego
     * @param precio indica el precio del videojuego
     * @param fechaSalida indica la fecha de salida del videojuego
     * @return
     * @throws SQLException
     */
    public int insertarVideojuego(String nombre, String desarrolladora, String genero, float precio, Timestamp fechaSalida) throws SQLException {
        if(connection == null) {
            return -1;
        }
        if(connection.isClosed()){
            return -1;
        }
        String consulta = "INSERT INTO videojuegos(nombre, desarrolladora, genero, precio, fecha_salida) VALUES (?,?,?,?,?)";
        PreparedStatement sentencia = connection.prepareStatement(consulta);
        sentencia.setString(1, nombre);
        sentencia.setString(2, desarrolladora);
        sentencia.setString(3, genero);
        sentencia.setFloat(4, precio);
        sentencia.setTimestamp(5, fechaSalida);

        int numeroRegistro = sentencia.executeUpdate();
        if(sentencia != null) {
            sentencia.close();
        }
        return numeroRegistro;
    }

    /**
     * Metodo para eliminar videojuegos de la base de datos
     * @param id para indicar que videojuego vamos a eliminar
     * @return devuleve el resultado de la consulta
     * @throws SQLException
     */
    public int eliminarVideojuego(int id) throws SQLException {
        if(connection == null) {
            return -1;
        }
        if(connection.isClosed()) {
            return -2;
        }
        String consulta = "DELETE FROM videojuegos WHERE id=?";
        PreparedStatement sentencia = connection.prepareStatement(consulta);
        sentencia.setInt(1, id);
        int resultado = sentencia.executeUpdate();
        if(sentencia != null) {
            sentencia.close();
        }
        return resultado;
    }

    /**
     * Metodo para modificar los datos de un videojuego
     * @param id indica el id del videojuego que es un campo unico
     * @param nombre indica el nombre del videojuego
     * @param desarrolladora indica el nombre de la desarrolladora
     * @param genero indica el genero al que pertenece el videojuego
     * @param precio indica el precio del videojuego
     * @param fechaSalida indica la fecha de salida del videojuego
     * @return
     * @throws SQLException
     */
    public int modificarVideojuego(int id, String nombre, String desarrolladora, String genero, float precio, Timestamp fechaSalida) throws SQLException {
        if(connection == null) {
            return -1;
        }
        if(connection.isClosed()) {
            return -2;
        }
        String consulta = "UPDATE videojuegos SET nombre=?, desarrolladora=?, genero=?, precio=?, fecha_salida=? WHERE id=?";
        PreparedStatement sentencia = connection.prepareStatement(consulta);
        sentencia.setString(1, nombre);
        sentencia.setString(2, desarrolladora);
        sentencia.setString(3, genero);
        sentencia.setFloat(4, precio);
        sentencia.setTimestamp(5, fechaSalida);
        sentencia.setInt(6, id);

        int resultado = sentencia.executeUpdate();
        if(sentencia == null) {
            sentencia.close();
            return -2;
        }
        return resultado;
    }

    /**
     * Metodo para crear la base de datos en caso de que no exista
     * @throws SQLException
     */
    public void crearBaseDeDatos() throws SQLException {
        String consulta = "CREATE DATABASE lista_videojuegos";
        connection = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306", "root", "");
        PreparedStatement sentencia = connection.prepareStatement(consulta);
        sentencia.executeUpdate();
        consulta = "use lista_videojuegos";
        sentencia = connection.prepareStatement(consulta);
        sentencia.executeUpdate();
        consulta = "CREATE TABLE videojuegos(\n" +
                "id int PRIMARY KEY AUTO_INCREMENT,\n" +
                "nombre VARCHAR(50),\n" +
                "desarrolladora VARCHAR(50),\n" +
                "genero VARCHAR(50),\n" +
                "precio float,\n" +
                "fecha_salida timestamp\n" +
                ")";
        sentencia = connection.prepareStatement(consulta);
        sentencia.executeUpdate();
    }

}
