package com.rubenescudero.unidad2;

import java.sql.SQLException;

/**
 * Clase principal donde creamos la vista, el modelo y se los pasamos al controlador.
 */
public class Main {
    public static void main(String[] args) throws SQLException {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();

        Controlador controlador = new Controlador(vista, modelo);
    }
}
