package com.rubenescudero.unidad2;

import javax.swing.event.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * Clase controlador para conectar la vista y el modelo
 */
public class Controlador implements ActionListener, TableModelListener, MouseListener {
    private Vista vista;
    private Modelo modelo;

    private enum estadoDeConexion {conectado, desconectado};
    private estadoDeConexion estado;

    /**
     * Constructor de controlador
     * @param vista vista que se va a usar
     * @param modelo modelo que se va a usar
     * @throws SQLException
     */
    public Controlador(Vista vista, Modelo modelo){
        this.vista = vista;
        this.modelo = modelo;
        estado = estadoDeConexion.desconectado;

        iniciarTabla();
        addActionListeners(this);
        addTableModelListeners(this);
        addMouse(this);
    }

    /**
     * Metodo para añadir ActionListener a los botones y opciones del menu
     * @param listener listener de la ventana
     */
    private void addActionListeners(ActionListener listener) {
        vista.btnBuscar.addActionListener(listener);
        vista.btnEliminar.addActionListener(listener);
        vista.btnNuevo.addActionListener(listener);
        vista.btnDeshacer.addActionListener(listener);
        vista.btnOrdenar.addActionListener(listener);
        vista.btnModificar.addActionListener(listener);
        vista.itemConectar.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.itemCrearBaseDeDatos.addActionListener(listener);
    }

    /**
     * Metodo para añadir el tableModelListeners a la tabla
     * @param listener
     */
    private void addTableModelListeners(TableModelListener listener){
        vista.dtm.addTableModelListener(listener);
    }

    /**
     * Metodo para añadir el mouseListener a la tabla
     * @param listener
     */
    private void addMouse(MouseListener listener){
        vista.table1.addMouseListener(listener);
    }

    /**
     * Metodo para iniciar la tabla y enviarle los datos de la cabecera.
     */
    private void iniciarTabla() {
        String[] cabecera = {"id", "nombre", "desarrolladora", "genero", "precio", "fecha de salida"};
        vista.dtm.setColumnIdentifiers(cabecera);
    }

    /**
     * Metodo para hacer la carga de datos de la base de datos
     * @param resultSet le pasamos el resultado de la consulta
     * @throws SQLException
     */
    private void cargarFilas(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[6];
        vista.dtm.setRowCount(0);
        while(resultSet.next()) {
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);
            fila[2] = resultSet.getObject(3);
            fila[3] = resultSet.getObject(4);
            fila[4] = resultSet.getObject(5);
            fila[5] = resultSet.getObject(6);

            vista.dtm.addRow(fila);
        }
        if(resultSet.last()) {
            try {
                vista.lblAction.setVisible(true);
                vista.lblAction.setText(resultSet.getRow() + " filas cargadas");
            } catch(NullPointerException e) {
                System.out.println("Filas cargadas");
            }
        }
    }

    /**
     * Metodo en el cual se recibe el actionCommand de los botones para realizar las distintas funciones del programa.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Nuevo":
                try {
                    modelo.insertarVideojuego(vista.txtNombre.getText(), vista.txtDesarrolladora.getText(), vista.txtGenero.getText(),
                            Float.parseFloat(vista.txtPrecio.getText()), Timestamp.valueOf(vista.fechaDeSalida.getDateTimePermissive()));
                    cargarFilas(modelo.obtenerDatos());
                    vista.txtNombre.setText("");
                    vista.txtDesarrolladora.setText("");
                    vista.txtGenero.setText("");
                    vista.txtPrecio.setText("");
                    vista.fechaDeSalida.clear();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "Eliminar":
                int filaBorrar = vista.table1.getSelectedRow();
                int idBorrar = (Integer)vista.dtm.getValueAt(filaBorrar, 0);
                try {
                    modelo.eliminarVideojuego(idBorrar);
                    vista.dtm.removeRow(filaBorrar);
                    vista.lblAction.setText("Se ha eliminado la fila");
                } catch (SQLException ex) {
                    ex.printStackTrace();
                } catch (NullPointerException ei){
                    System.out.println("Filas cargadas");
                }
                break;
            case "Salir":
                System.exit(0);
                break;
            case "Buscar":
                buscarPorCampo();
                break;
            case "Deshacer":
                try {
                    cargarFilas(modelo.obtenerDatos());
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "Conectar":
                try {
                    modelo.conectar();
                    cargarFilas(modelo.obtenerDatos());
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "Crear":
                try {
                    modelo.crearBaseDeDatos();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "Ordenar":
                ordenarPorCategoria();
                break;
            case "Modificar":
                int fila = vista.table1.getSelectedRow();
                int id = (int)vista.dtm.getValueAt(fila, 0);
                String nombre = vista.txtNombre.getText();
                String desarrollador = vista.txtDesarrolladora.getText();
                String genero = vista.txtGenero.getText();
                float precio = Float.parseFloat(vista.txtPrecio.getText());
                Timestamp fecha = Timestamp.valueOf(vista.fechaDeSalida.getDateTimePermissive());
                try {
                    modelo.modificarVideojuego(id, nombre, desarrollador, genero, precio, fecha);
                    cargarFilas(modelo.obtenerDatos());
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
        }
    }

    /**
     * Metodo para ordenar datos por distintas categorias
     */
    private void ordenarPorCategoria() {
        try {
            if(vista.comboBoxOrdenar.getSelectedItem().equals("Nombre")){
                cargarFilas(modelo.ordernarAlfabeticamente());
            }
            else if(vista.comboBoxOrdenar.getSelectedItem().equals("Desarrolladora")){
                cargarFilas(modelo.ordernarDesarrolladora());
            }
            else if(vista.comboBoxOrdenar.getSelectedItem().equals("Precio")) {
                cargarFilas(modelo.ordernarPrecio());
            }
        } catch (SQLException ex) {
                ex.printStackTrace();
        }
    }

    /**
     * Metodo para buscar los datos por distintos campos.
     */
    private void buscarPorCampo() {
        if(vista.comboBox1.getSelectedItem().equals("Nombre")){
            String nombre = vista.txtBuscar.getText();
            try {
                cargarFilas(modelo.buscarPorNombre(nombre));
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        else{
            String id = vista.txtBuscar.getText();
            try {
                cargarFilas(modelo.buscarPorId(id));
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void tableChanged(TableModelEvent e) {

    }

    /**
     * Metodo que escucha la table y devuelve sus valos a los campos para ser modificados
     * @param e
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        int fila = vista.table1.getSelectedRow();
        String nombre = (String)vista.dtm.getValueAt(fila, 1);
        vista.txtNombre.setText(nombre);
        String desarrolladora = (String)vista.dtm.getValueAt(fila, 2);
        vista.txtDesarrolladora.setText(desarrolladora);
        String genero = (String)vista.dtm.getValueAt(fila, 3);
        vista.txtGenero.setText(genero);
        float precio = (float)vista.dtm.getValueAt(fila, 4);
        vista.txtPrecio.setText(String.valueOf(precio));
        Timestamp fechaSalida = (Timestamp)vista.dtm.getValueAt(fila, 5);
        vista.fechaDeSalida.setDateTimePermissive(fechaSalida.toLocalDateTime());
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
