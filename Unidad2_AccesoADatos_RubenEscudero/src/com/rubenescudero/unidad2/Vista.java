package com.rubenescudero.unidad2;

import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * Clase vista donde tenemos los elemntos que se visualizan en nuestra pantallla
 */
public class Vista {
    JFrame frame;
    JTextField txtNombre;
    JTextField txtDesarrolladora;
    JTextField txtGenero;
    JTextField txtPrecio;
    JButton btnBuscar;
    JTextField txtBuscar;
    JButton btnNuevo;
    JButton btnEliminar;
    JTable table1;
    DateTimePicker fechaDeSalida;
    private JPanel panel;
    JComboBox comboBox1;
    JButton btnDeshacer;
    JButton btnOrdenar;
    JComboBox comboBoxOrdenar;
    JButton btnModificar;

    JLabel lblAction;
    DefaultTableModel dtm;
    JMenuItem itemConectar;
    JMenuItem itemSalir;
    JMenuItem itemCrearBaseDeDatos;
    JMenu menu;
    JMenuBar jMenuBar;

    /**
     * Constructor de la clase Vista donde le asginamos las porpiedades a la ventana
     */
    public Vista() {
        frame = new JFrame("Conexion con base de datos");
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        dtm = new DefaultTableModel();
        table1.setModel(dtm);
        crearMenu();
        frame.setJMenuBar(jMenuBar);
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Metodo que inicializa las variables necesarias para crear un menu y se las agrega al JMenuBar
     */
    private void crearMenu() {
        jMenuBar = new JMenuBar();
        menu = new JMenu("Opciones");
        jMenuBar.add(menu);

        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("Conectar");
        menu.add(itemConectar);
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");
        menu.add(itemSalir);
        itemCrearBaseDeDatos = new JMenuItem("Crear base de datos");
        itemCrearBaseDeDatos.setActionCommand("Crear");
        menu.add(itemCrearBaseDeDatos);
    }

}
