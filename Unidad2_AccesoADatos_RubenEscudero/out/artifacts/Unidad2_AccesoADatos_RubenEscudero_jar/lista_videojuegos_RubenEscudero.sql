CREATE DATABASE lista_videojuegos;

use lista_videojuegos;

CREATE TABLE videojuegos(
id int PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR(50),
desarrolladora VARCHAR(50),
genero VARCHAR(50),
precio float,
fecha_salida timestamp
);

CREATE TABLE desarrollador(
id INT PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR(50),
pais VARCHAR(50),
fecha_fundacion DATE,
id_desarrollador INT,
FOREIGN KEY (id_desarrollador) REFERENCES videojuegos(id)
);


